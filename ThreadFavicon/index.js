'use strict';

var staticDom = require('../../engine/domManipulator/static.js');
var templateHandler = require('../../engine/templateHandler');
exports.engineVersion = '2.6';

exports.init = function() {
	var threadPage = templateHandler.pageTests.filter(function(page) { return page.template == 'threadPage' })[0];
	
	threadPage.prebuiltFields['favicon'] = "href";

	var originalThread = staticDom.setThreadTitle;

	staticDom.setThreadTitle = function(document, threadData){
		var favicon = "/favicon.png";
		for(let file of threadData.files){
			if(
				(file.width === 32 || file.width === 16) && 
				(file.height === 32 || file.height === 16) && 
				file.mime === "image/png" &&
				file.originalName.includes("favicon")
				) {
				favicon = file.path;
			}
		}
  		document = document.replace('__favicon_href__',  favicon);
  		return originalThread(document, threadData);
  	};
}