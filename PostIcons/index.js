'use strict';

var posting = require('../../engine/postingOps');
var edit = require('../../engine/modOps').edit;
var postsTable = require('../../db').conn().collection('posts');
var threadsTable = require('../../db').conn().collection('threads');

exports.engineVersion = '2.7';

var post = posting.post;
var thread = posting.thread;
var common = posting.common;

var iconBoard = "icon";
var endGex = "\.(png|gif|jpg|jpeg)";
var promises = [];
var regularExp = /:[a-zA-Z0-9_-]{1,30}:/g;

function escapeRegex(string) {
    return string.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
}

exports.getPromises = function(m,name){
  if(!name) name = "Anonymous";
  m = escapeRegex(m.replaceAll(":","")) + endGex;
  promises.push(postsTable.find({
    boardUri:iconBoard,
    name:{$in:["Anonymous",name]},
    files:{$exists:true},
    'files.originalName':new RegExp(m),
    trash:{$exists: false}
  }).sort({creation:1}).toArray());
  promises.push(threadsTable.find({
    boardUri:iconBoard,
    name:{$in:["Anonymous",name]},
    files:{$exists:true},
    'files.originalName':new RegExp(m),
    trash:{$exists: false}
  }).sort({creation:1}).toArray());
}
exports.convertMarkdown = function(m,postResult,threadResult,name){
  if(!name) name = "Anonymous";
  let filename = escapeRegex(m.replaceAll(":","")) + endGex;
  var result = [];
  result = result.concat(postResult).concat(threadResult);
  if(result && result.length){
    let selectedFile = "";
    for(let i=0;i<result.length;i++){
      if(!result[i]) continue;
      for(let x=0;x<result[i].files.length;x++){
        if(result[i].files[x].originalName.match(new RegExp(filename))){
          if(!selectedFile){
            selectedFile = result[i].files[x];
            selectedFile["name"] = result[i].name;
            selectedFile["datetime"] = Date.parse(result[i].creationDate);
          } else if(selectedFile.name === "Anonymous" && result[i].name !== "Anonymous"){
            selectedFile = result[i].files[x];
            selectedFile["name"] = result[i].name;
            selectedFile["datetime"] = Date.parse(result[i].creationDate);
          } else if(selectedFile.name === result[i].name && Date.parse(result[i].name) < selectedFile.datetime){
            selectedFile = result[i].files[x];
            selectedFile["name"] = result[i].name;
            selectedFile["datetime"] = Date.parse(result[i].creationDate);
          }
        }
      }
    }
    if(selectedFile) return "<a href=\"" + selectedFile["path"] + "\"><img title=\"" + m + "\" class=\"postIcon\" src=\"" + selectedFile["thumb"] + "\"></a>";
  }
  return m;
};


exports.init = function() {

  var originalPost = post.createPost;

  post.createPost = function(req, parameters, newFiles, userData, postId,
      thread, board, wishesToSign, enabledCaptcha, cb
  ){
    promises = [];
    parameters.markdown.replaceAll(regularExp, m => {
      exports.getPromises(m,parameters.name);
    });
    Promise.all(promises).then(results => {
      parameters.markdown = parameters.markdown.replaceAll(regularExp, m => {
        return exports.convertMarkdown(m,results.shift(),results.shift(),parameters.name);
      });
      originalPost(req, parameters, newFiles, userData, postId,
        thread, board, wishesToSign, enabledCaptcha, cb
      );
    });
  };

  var originalThread = thread.createThread;

  thread.createThread = function(req, userData, parameters, newFiles, board,
    threadId, wishesToSign, enabledCaptcha, callback
  ){
    promises = [];
    parameters.markdown.replaceAll(regularExp, m => {
      exports.getPromises(m,parameters.name);
    });
    Promise.all(promises).then(results => {
      parameters.markdown = parameters.markdown.replaceAll(regularExp, m => {
        return exports.convertMarkdown(m,results.shift(),results.shift(),parameters.name);
      });
      originalThread(req, userData, parameters, newFiles, board,
        threadId, wishesToSign, enabledCaptcha, callback
      );
    });
  };

  var originalEdit = edit.recordEdit;
  edit.recordEdit= function(parameters, login, language, callback) {
    promises = [];
    var fullPost = postsTable.findOne({
      boardUri:parameters.boardUri,
      postId:parseInt(parameters.postId)
    });
    var threadPost = threadsTable.findOne({
      boardUri:parameters.boardUri,
      threadId:parseInt(parameters.threadId)
    });
    Promise.all([fullPost,threadPost]).then(r => {
      if(r[0]) parameters.name = r[0].name;
      else parameters.name = r[1].name;
      parameters.markdown.replaceAll(regularExp, m => {
        exports.getPromises(m,parameters.name);
      });
      Promise.all(promises).then(results => {
        parameters.markdown = parameters.markdown.replaceAll(regularExp, m => {
          return exports.convertMarkdown(m,results.shift(),results.shift(),parameters.name);
        });
        originalEdit(parameters, login, language, callback);
      });
    });
  };
};
