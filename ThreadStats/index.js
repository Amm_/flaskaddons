'use strict';

var staticDom = require('../../engine/domManipulator/static.js');
var jsonBuilder = require('../../engine/jsonBuilder.js');
var templateHandler = require('../../engine/templateHandler');
exports.engineVersion = '2.7';

var threadFields = {
	"labelReplies": "inner",
	"labelImages": "inner",
	"labelPage": "inner",
};

exports.init = function() {
	var threadPage = templateHandler.pageTests.filter(function(page) { return page.template == 'threadPage' })[0];
	
	threadPage.prebuiltFields = {...threadPage.prebuiltFields,...threadFields};

	var originalThread = staticDom.setThreadTitle;

	staticDom.setThreadTitle = function(document, threadData){
  		document = document.replace('__labelReplies_inner__',  threadData.postCount || 0);
  		document = document.replace('__labelImages_inner__',  threadData.fileCount || 0);
  		document = document.replace('__labelPage_inner__',  threadData.page || 1);
  		return originalThread(document, threadData);
  	};

  	var originalThreadObject = jsonBuilder.getThreadObject;

  	jsonBuilder.getThreadObject = function(thread, posts, board, modding, userRole){
  		var threadObject = originalThreadObject(thread, posts, board, modding, userRole);
  		if (posts && posts.length < thread.postCount) {
		    var displayedImages = 0;

		    for (var i = 0; i < posts.length; i++) {
		      var post = posts[i];

		      if (post.files) {

		        displayedImages += post.files.length;
		      }
		    }
		    threadObject.postCount = posts.length;
		    threadObject.fileCount = displayedImages;
		} else {
			threadObject.postCount = thread.postCount;
			threadObject.fileCount = thread.fileCount;
		}
		threadObject.page = thread.page;

  		return threadObject;
  	};
}