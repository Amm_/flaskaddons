'use strict';

var postsTable = require('../../db').conn().collection('posts');
var threadsTable = require('../../db').conn().collection('threads');
var posting = require('../../engine/postingOps');
var https = require('https');

var post = posting.post;
var thread = posting.thread;
var common = posting.common;
var mintime = 3*60*60*1000;

exports.engineVersion = '2.7';

exports.sendWebhook = function(text, url = '/api/webhooks/879096499668144168/03h7wbPk0cGsu6IWkGciWL02oPYp2Zdabf620pS_4cOa1J2Ew4RXTaCXZW4oQPBOA58r'){
	var data = new TextEncoder().encode(
	  	JSON.stringify({
	    	content: text
	  	})
	);
	var options = {
	  	hostname: 'discord.com',
	  	port: 443,
	  	path: url,
	  	method: 'POST',
	  	headers: {
		    'Content-Type': 'application/json',
		    'Content-Length': data.length
		}
	};
	var test = https.request(options, res => {
	  	console.log(`statusCode: ${res.statusCode}`)

	  	res.on('data', d => {
	    	process.stdout.write(d)
	  	});
	});

	test.on('error', error => {
	  console.error(error);
	});

	test.write(data);
	test.end();
}

exports.setCooldown = function(threadId,boardUri){
	threadsTable.updateOne(
		{
			threadId:threadId,
			boardUri:boardUri
		},
		{$set: {
			webhookCooldown: Date.now()
		}
	});
}

exports.init = function() {
	common.postingParameters[1].length = 200;
	var originalPost = post.createPost;

  	post.createPost = function(req, parameters, newFiles, userData, postId,
      	thread, board, wishesToSign, enabledCaptcha, cb
  	){
  		if(parameters.boardUri === "quest"){
  			threadsTable.findOne({
			    boardUri : parameters.boardUri,
			    trash : { $ne : true },
				threadId : parameters.threadId
			}, {
			    projection : {
			      	webhookCooldown : 1,
			      	name: 1,
			      	email: 1,
			    }
			}, function gotCooldown(error,  thread){
				if(error) return;

				if(thread.name === parameters.name && (!thread.webhookCooldown || Date.now() - thread.webhookCooldown > mintime )){
					exports.sendWebhook("New Post: https://getyeflask.net/quest/res/" + parameters.threadId + ".html");
					if(thread.email) exports.sendWebhook("New Post: https://getyeflask.net/quest/res/" + parameters.threadId + ".html", thread.email);
					exports.setCooldown(parameters.threadId,parameters.boardUri);
				}
			});
		}

      	originalPost(req, parameters, newFiles, userData, postId,
        	thread, board, wishesToSign, enabledCaptcha, cb
      	);
  	};

  	var originalThread = thread.createThread;

  	thread.createThread = function(req, userData, parameters, newFiles, board,
    	threadId, wishesToSign, enabledCaptcha, callback
  	){
    	if(parameters.boardUri === "quest"){
    		console.log(parameters);
  			exports.sendWebhook("New Thread: https://getyeflask.net/quest/res/" + threadId + ".html");
  			if(parameters.email) exports.sendWebhook("New Thread: https://getyeflask.net/quest/res/" + threadId + ".html", parameters.email);
  			setTimeout(function(){
  				exports.setCooldown(parameters.threadId,parameters.boardUri);
  			},5*1000);
		}
  		originalThread(req, userData, parameters, newFiles, board,
    		threadId, wishesToSign, enabledCaptcha, callback
  		);
  	};
}