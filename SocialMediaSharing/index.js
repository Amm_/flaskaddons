'use strict';

var staticDom = require('../../engine/domManipulator/static.js');
var common = require('../../engine/domManipulator/common.js');
var templateHandler = require('../../engine/templateHandler');
exports.engineVersion = '2.7';

var threadFields = {
	"socialTitle": "content",
	"socialURL": "content",
	"socialDescription": "content",
	"socialImage": "content",
};
var pageFields = {
	"socialTitle": "content",
	"socialURL": "content",
	"socialDescription": "content",
};

exports.init = function() {
	var boardPage = templateHandler.pageTests.filter(function(page) { return page.template == 'boardPage' })[0];
	var threadPage = templateHandler.pageTests.filter(function(page) { return page.template == 'threadPage' })[0];

	boardPage.prebuiltFields = {...boardPage.prebuiltFields,...pageFields};
	threadPage.prebuiltFields = {...threadPage.prebuiltFields,...threadFields};
	templateHandler.simpleAttributes["content"] = true;

	var originalThread = staticDom.setThreadTitle;

	staticDom.setThreadTitle = function(document, threadData) {
		var title ="";
		var description = "";
		if (threadData.subject) {
    		title += common.clean(threadData.subject.substring(0, 60));
		} else {
		    title += common.clean(threadData.message.substring(0, 60));
		}
		if(threadData.message.length > 150){
			description = common.clean(threadData.message.substring(0, 150));
		} else {
			description = common.clean(threadData.message + "...");
		}

		document = document.replace("__socialTitle_content__",title)
			.replace("__socialURL_content__","https://www.example.com/" + threadData.boardUri + "/" + threadData.threadId)
			.replace("__socialDescription_content__",description);

		if(threadData.files.length){
			document = document.replace("__socialImage_content__", threadData.files[0].thumb);
		}
		return originalThread(document,threadData);
	}

	var originalPage = staticDom.writePage;

	staticDom.writePage = function(boardUri, page, boardData, language, document, callback) {

		document = document.replace("__socialTitle_content__","/" + boardData.boardUri + "/ - " + boardData.boardName)
			.replace("__socialURL_content__","https://www.example.com/" + boardData.boardUri + "/")
			.replace("__socialDescription_content__",boardData.boardDescription);
		originalPage(boardUri, page, boardData, language, document, callback);
	}

}
