'use strict';

var editForm = require('../../form/edit');
var saveEditForm = require('../../form/saveEdit');
var modEdit = require('../../engine/modOps').edit;
var common = require('../../engine/modOps').common;
var miscOps  = require('../../engine/miscOps');
var lang  = require('../../engine/langOps').languagePack;
var r9k  = require('../../engine/r9k');
var formOps = require('../../engine/formOps');
var boards = require('../../db').conn().collection('boards');
var posts = require('../../db').conn().collection('posts');
var threads = require('../../db').conn().collection('threads');

var logger = require('../../logger');
var url = require('url');
exports.postTimeLimit = 20*60*1000;

exports.engineVersion = '2.7';

exports.init = function() {
	var originalProcess = editForm.process;
	
	editForm.process = function(req, res) {
		formOps.getAuthenticatedPost(req, res, false, function gotData(auth, user) {
	    	var parameters = url.parse(req.url, true).query;
	    	if(!user) user = logger.ip(req);
	    	editForm.getPostingToEdit(user, parameters, res, auth, req.language);
		}, true, true);
	};

	var originalEdit = modEdit.getPostingToEdit; 

	modEdit.getPostingToEdit = function(userData, parameters, language, callback) {
	  	if(userData && userData.login) originalEdit(userData, parameters, language, callback);
	  	else exports.ipEdit(userData, parameters, language, callback);
	};



	var originalSaveProcess = saveEditForm.process;
	
	saveEditForm.process = function(req, res) {
		formOps.getAuthenticatedPost(req, res, true, function gotData(auth, userData,parameters) {
	    	if(!userData) userData = logger.ip(req);
	    	saveEditForm.saveEdit(parameters, userData, res, auth, req.language, formOps.json(req));
		}, true);
	};

	var originalSaveEdit = modEdit.saveEdit;

	modEdit.saveEdit = function(userData, parameters, language, callback) {
		if(userData && userData.login) originalSaveEdit(userData, parameters, language, callback);
	  	else exports.ipSaveEdit(userData, parameters, language, callback);
	}
};

exports.ipEdit = function(userData, parameters, language, callback){

	parameters.boardUri = parameters.boardUri.toString();

	boards.findOne({
    	boardUri : parameters.boardUri
  	}, function gotBoard(error, board) {
    if (error) {
      callback(error);
    } else if (!board) {
      callback(lang(language).errBoardNotFound);
    } else {

      var collectionToUse;
      var query;

      if (parameters.postId) {

        query = {
          postId : +parameters.postId
        };
        collectionToUse = parameters.postId ? posts : threads;
      } else {
        collectionToUse = threads;

        query = {
          threadId : +parameters.threadId
        };

      }

      query.boardUri = parameters.boardUri;

      collectionToUse.findOne(query, {
        subject : 1,
        message : 1,
        ip: 1,
        creation: 1
      }, function gotPosting(error, posting) {
        if (error) {
          	callback(error);
        } else if (!posting) {
          	callback(lang(language).errPostingNotFound);
        } else if(posting.ip && (posting.ip.toString() !== userData.toString())){
        	callback("Error: Bad IP");
        } else if((new Date()).getTime() - new Date(posting.creation).getTime() >= exports.postTimeLimit){
        	callback("Error: It's been too long since the post was created");
        } else {
          	callback(null, posting);
        }
      });
    }

  });

};

exports.ipSaveEdit = function(userData, parameters, language, callback) {
	miscOps.sanitizeStrings(parameters, modEdit.editArguments);

	parameters.hash = r9k.getMessageHash(parameters.message);

	parameters.boardUri = parameters.boardUri.toString();

	boards.findOne({
	    boardUri : parameters.boardUri
	}, function gotBoard(error, board) {
	    if (error) {
	      	callback(error);
	    } else if (!board) {
	      	callback(lang(language).errBoardNotFound);
	    }  else {
	    	var collectionToUse;
		    var query;

		    if (parameters.postId) {

		        query = {
		          	postId : +parameters.postId
		        };
		        collectionToUse = parameters.postId ? posts : threads;
		    } else {
		        collectionToUse = threads;

		        query = {
		          	threadId : +parameters.threadId
		        };

		    }

		    query.boardUri = parameters.boardUri;

		    collectionToUse.findOne(query, {
		        subject : 1,
		        message : 1,
		        ip: 1,
		        creation: 1
		    }, function gotPosting(error, posting) {
		        if (error) {
		          	callback(error);
		        } else if (!posting) {
		          	callback(lang(language).errPostingNotFound);
		        } else if(posting.ip && (posting.ip.toString() !== userData.toString())){
		        	callback("Error: Bad IP");
		        } else if((new Date()).getTime() - new Date(posting.creation).getTime() >= exports.postTimeLimit){
		        	callback("Error: It's been too long since the post was created");
		        } else {
		          	r9k.check(parameters, board, language, function checked(error) {
				        if (error) {
				          	callback(error);
				        } else {
				        	userData.login="this user";
				          	modEdit.getMarkdown(parameters, userData, board, language, callback);
				        }
			      	});
		        }
		    });


	      	
	    }
	 });
};